## Responsive Website 
### Brighton Times

Here is the Brighton Times Project,
it is a responsive website optimized for mobile, tablet and desktop screens.

This site is deployed on 
[On Netlify](https://frosty-jennings-d81592.netlify.com/)

To run this :

* Clone this repo
* Open index.html

OR 

* Clone this repo 
* If you have node, run command "npm i -g live-server"
* Open terminal in this repo
* Run "npm run dev"

OR 

* Install Live Server Extension on VSCode
* and click Go Live.

To Build this: 

* Run Command "npm install -g parcel-bundler"
* Then "parcel build index.html"